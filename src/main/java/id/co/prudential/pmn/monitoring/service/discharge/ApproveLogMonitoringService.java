package id.co.prudential.pmn.monitoring.service.discharge;

import id.co.prudential.pmn.monitoring.model.Action;
import id.co.prudential.pmn.monitoring.model.PmnDocsum;
import id.co.prudential.pmn.monitoring.model.PmnDocsumHistory;
import id.co.prudential.pmn.monitoring.model.PmnTransmon;
import id.co.prudential.pmn.monitoring.model.PmnTransmonHistory;
import id.co.prudential.pmn.monitoring.repository.BaseRepository;
import id.co.prudential.pmn.monitoring.repository.PmnTransmonRepository;
import id.co.prudential.pmn.monitoring.repository.PmnTransmonHistoryRepository;

public class ApproveLogMonitoringService implements DischargeMonitoringInterface{

    private Action action;
    private PmnTransmon transMon;
    private PmnTransmonHistory transmonHistory;
    private PmnDocsum docsum;
    private PmnDocsumHistory docsumHistory;

    BaseRepository<PmnTransmon> transMonRepo = new PmnTransmonRepository();
    BaseRepository<PmnTransmonHistory> transmonHistoryRepo = new PmnTransmonHistoryRepository();

    @Override
    public void processMonitoring(Action action) {
        this.action = action;
        updateTransmon();
        updateTransmonHistory();
        updateHopsum();
        updateHopsumHistory();
        updateDocsum();
        updateDocsumHistory();
    }

    @Override
    public void updateTransmon() {
        // TODO Auto-generated method stub
        //inquiry data2 pendukung untuk transmon

        //setelah data pendukung sudah didapat, susun model transmon
        transMon = new PmnTransmon(
            null,
            "reffId",
            "admissionId",
            "policyNumber",
            "patientName"
        );
        transMon = transMonRepo.save(transMon);
    }

    @Override
    public void updateTransmonHistory() {
        // TODO Auto-generated method stub
        transmonHistoryRepo.saveHistory(transMon);
    }

    @Override
    public void updateHopsum() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void updateHopsumHistory() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void updateDocsum() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void updateDocsumHistory() {
        // TODO Auto-generated method stub
        
    }

}
