package id.co.prudential.pmn.monitoring.repository;

import java.util.Optional;

import id.co.prudential.pmn.monitoring.model.PmnTransmon;

public class PmnTransmonRepository implements BaseRepository<PmnTransmon> {

    @Override
    public Optional<PmnTransmon> getById(int id) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk mendapatkan PmnTransMon by id
        //kemudian return
        return Optional.empty();
    }

    @Override
    public PmnTransmon save(PmnTransmon t) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk insert ke table pmn_transmon
        return null;
    }

    @Override
    public PmnTransmon update(PmnTransmon t) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk update table pmn_transmon
        return null;
    }

    @Override
    public void delete(PmnTransmon t) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk delete table pmn_transmon
    }
    
    public Optional<PmnTransmon> getByReffId(String reffId){
        return null;
    }

    @Override
    public PmnTransmon saveHistory(Object transMon) {
        // TODO Auto-generated method stub
        return null;
    }
}
