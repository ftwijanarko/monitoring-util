package id.co.prudential.pmn.monitoring.model;

public class Action {
    private String action;
    private String reffId;
    private String hospitalId;

    public Action() {
    }

    public Action(String action, String reffId, String hospitalId) {
        this.action = action;
        this.reffId = reffId;
        this.hospitalId = hospitalId;
    }

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getReffId() {
        return reffId;
    }
    public void setReffId(String reffId) {
        this.reffId = reffId;
    }
    public String getHospitalId() {
        return hospitalId;
    }
    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }
}
