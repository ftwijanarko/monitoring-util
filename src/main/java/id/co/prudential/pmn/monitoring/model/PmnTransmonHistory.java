package id.co.prudential.pmn.monitoring.model;

public class PmnTransmonHistory extends PmnTransmon{
    private Integer transmonHistoryId;

    public PmnTransmonHistory(int transmonId, String reffId, String admissionId, String policyNumber,
            String patientName, Integer transmonHistoryId) {
        super(transmonId, reffId, admissionId, policyNumber, patientName);
        this.transmonHistoryId = transmonHistoryId;
    }

    public int getTransmonHistoryId() {
        return transmonHistoryId;
    }

    public void setTransmonHistoryId(int transmonHistoryId) {
        this.transmonHistoryId = transmonHistoryId;
    }
    
}
