package id.co.prudential.pmn.monitoring.repository;

import java.util.Optional;

import id.co.prudential.pmn.monitoring.model.PmnTransmon;
import id.co.prudential.pmn.monitoring.model.PmnTransmonHistory;

public class PmnTransmonHistoryRepository implements BaseRepository<PmnTransmonHistory> {

    @Override
    public Optional<PmnTransmonHistory> getById(int id) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk mendapatkan PmnTransMon by id
        return Optional.empty();
    }

    @Override
    public PmnTransmonHistory save(PmnTransmonHistory t) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk insert ke table pmn_transmon_history
        return null;
        
    }

    @Override
    public PmnTransmonHistory update(PmnTransmonHistory t) {
        // TODO Auto-generated method stub
         //lakukan rest service ke pmn-transaction api untuk update table pmn_transmon_history
        return null;
        
    }

    @Override
    public void delete(PmnTransmonHistory t) {
        // TODO Auto-generated method stub
        //lakukan rest service ke pmn-transaction api untuk delete table pmn_transmon_history
    }
    
    public PmnTransmonHistory saveHistory(Object pmnTransmonObj){
        PmnTransmon pmnTransmon = PmnTransmon.class.cast(pmnTransmonObj);
        PmnTransmonHistory pmnTransmonHistory = new PmnTransmonHistory(
            pmnTransmon.getTransmonId(), 
            pmnTransmon.getReffId(), 
            pmnTransmon.getAdmissionId(), 
            pmnTransmon.getPolicyNumber(), 
            pmnTransmon.getPatientName(), 
            null
        );
        return save(pmnTransmonHistory);
    }

}
