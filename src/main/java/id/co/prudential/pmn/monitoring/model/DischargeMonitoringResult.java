package id.co.prudential.pmn.monitoring.model;

public class DischargeMonitoringResult {
    private int totalClaimReview;
    private int totalPendingHospital;
    private int totalCancel;

    public DischargeMonitoringResult(){

    }

    public DischargeMonitoringResult(int totalClaimReview, int totalPendingHospital, int totalCancel) {
        this.totalClaimReview = totalClaimReview;
        this.totalPendingHospital = totalPendingHospital;
        this.totalCancel = totalCancel;
    }

    public int getTotalClaimReview() {
        return totalClaimReview;
    }
    public void setTotalClaimReview(int totalClaimReview) {
        this.totalClaimReview = totalClaimReview;
    }
    public int getTotalPendingHospital() {
        return totalPendingHospital;
    }
    public void setTotalPendingHospital(int totalPendingHospital) {
        this.totalPendingHospital = totalPendingHospital;
    }
    public int getTotalCancel() {
        return totalCancel;
    }
    public void setTotalCancel(int totalCancel) {
        this.totalCancel = totalCancel;
    }

    
}
