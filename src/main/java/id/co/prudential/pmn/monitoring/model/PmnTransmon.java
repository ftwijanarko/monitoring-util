package id.co.prudential.pmn.monitoring.model;

public class PmnTransmon {
    private Integer transmonId;
    private String reffId;
    private String admissionId;
    private String policyNumber;
    private String patientName;

    public PmnTransmon() {
    }

    public PmnTransmon(Integer transmonId, String reffId, String admissionId, String policyNumber, String patientName) {
        this.transmonId = transmonId;
        this.reffId = reffId;
        this.admissionId = admissionId;
        this.policyNumber = policyNumber;
        this.patientName = patientName;
    }

    public int getTransmonId() {
        return transmonId;
    }
    public void setTransmonId(int transmonId) {
        this.transmonId = transmonId;
    }
    public String getReffId() {
        return reffId;
    }
    public void setReffId(String reffId) {
        this.reffId = reffId;
    }
    public String getAdmissionId() {
        return admissionId;
    }
    public void setAdmissionId(String admissionId) {
        this.admissionId = admissionId;
    }
    public String getPolicyNumber() {
        return policyNumber;
    }
    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }
    public String getPatientName() {
        return patientName;
    }
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }


    
}
