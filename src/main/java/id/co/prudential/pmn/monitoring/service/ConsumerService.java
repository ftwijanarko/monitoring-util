package id.co.prudential.pmn.monitoring.service;

import id.co.prudential.pmn.monitoring.constant.ActionConstant;
import id.co.prudential.pmn.monitoring.model.Action;
import id.co.prudential.pmn.monitoring.service.discharge.ApproveLogMonitoringService;
import id.co.prudential.pmn.monitoring.service.discharge.DischargeMonitoringInterface;

public class ConsumerService {
    
    public void consumer(Action action){

        DischargeMonitoringInterface monitoringInterface;
        
        switch(action.getAction()) {
            case ActionConstant.APPROVE_LOG:
                monitoringInterface = new ApproveLogMonitoringService();
                break;
            default:
                monitoringInterface = null;
        }

        if(monitoringInterface != null){
            monitoringInterface.processMonitoring(action);
            monitoringInterface.calculateMonitoringSummary(action);
        }else{
            
        }
        
    }
}
