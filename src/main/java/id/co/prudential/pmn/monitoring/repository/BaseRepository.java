package id.co.prudential.pmn.monitoring.repository;

import java.util.Optional;

public interface BaseRepository<T> {
    Optional<T> getById(int id);

    T save(T t);
    
    T update(T t);
    
    void delete(T t);

    T saveHistory(Object transMon);
}
