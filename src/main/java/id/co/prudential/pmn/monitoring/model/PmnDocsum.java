package id.co.prudential.pmn.monitoring.model;

public class PmnDocsum {
    private int id;
    private String admissionId;
    private String docnumId;
    private int transmonHistoryId;

    public PmnDocsum() {
    }
    
    public PmnDocsum(int id, String admissionId, String docnumId, int transmonHistoryId) {
        this.id = id;
        this.admissionId = admissionId;
        this.docnumId = docnumId;
        this.transmonHistoryId = transmonHistoryId;
    }

    
    public int getId() {
        return id;
    }
    public String getAdmissionId() {
        return admissionId;
    }
    public String getDocnumId() {
        return docnumId;
    }
    public int getTransmonHistoryId() {
        return transmonHistoryId;
    }

    
}
