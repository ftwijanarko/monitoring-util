package id.co.prudential.pmn.monitoring.service.discharge;

import id.co.prudential.pmn.monitoring.model.Action;
import id.co.prudential.pmn.monitoring.model.DischargeMonitoringResult;
import id.co.prudential.pmn.monitoring.service.calculation.CalculationMonitoringInterface;
import id.co.prudential.pmn.monitoring.service.calculation.DischargeCalculationMonitoringService;

public interface DischargeMonitoringInterface {
    
    CalculationMonitoringInterface<DischargeMonitoringResult> dischargeCalculationMonitoringService = new DischargeCalculationMonitoringService();
    
    public void processMonitoring(Action action);
    public void updateTransmon();
    public void updateTransmonHistory();
    public void updateHopsum();
    public void updateHopsumHistory();
    public void updateDocsum();
    public void updateDocsumHistory();

    default void calculateMonitoringSummary(Action action){
        DischargeMonitoringResult dischargeMonitoringResult = new DischargeMonitoringResult();
        dischargeMonitoringResult = dischargeCalculationMonitoringService.calculateMonitoring(action.getHospitalId());
        produceToKafka(dischargeMonitoringResult);
    }
    
    default void produceToKafka(DischargeMonitoringResult dischargeMonitoringResult){
        //send ke kafka interface untuk di broadcast
    };
}
