package id.co.prudential.pmn.monitoring.service.calculation;

public interface CalculationMonitoringInterface<T> {
    T calculateMonitoring(String hospitalId);
}
