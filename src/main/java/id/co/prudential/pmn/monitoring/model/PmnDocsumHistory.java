package id.co.prudential.pmn.monitoring.model;

public class PmnDocsumHistory extends PmnDocsum{
    private int docsumHistoryId;

    public PmnDocsumHistory() {
    }

    public PmnDocsumHistory(int docsumHistoryId) {
        this.docsumHistoryId = docsumHistoryId;
    }

    public int getDocsumHistoryId() {
        return docsumHistoryId;
    }

    public void setDocsumHistoryId(int docsumHistoryId) {
        this.docsumHistoryId = docsumHistoryId;
    }

    
}
